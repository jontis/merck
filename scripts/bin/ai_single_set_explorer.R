#!/usr/bin/Rscript
#TODO: report
#TODO: individual parameters for the sets?
#TODO: dream up and evaluate more environmentals
#TODO: make a function that pulls and stores a sample of worst and best predictions for examination
#TODO: weight the training set? we know that the test set comes AFTER the training set in real life experiments
#TODO: denoise?

rm(list=ls(all=TRUE))                   #clear data
options("max.print" = 150)              #limit output to console

#set working directory
setwd("/home/jonathan/kaggle/merck/scripts/")

require(Matrix)                               #sparse matrices
require(stringr)                              #load 'stringr' for simpler string handling
#require(methods)                              #load methods for OO programming
require(multicore)
require(doMC)

#configurations
source('lib/configs.R')

if(general_config$general$cores > 1){registerDoMC(cores=general_config$general$cores)}else{registerDoSEQ()}      #initiate multicore

#override, just run one set with these special options
general_config$general$source_data_numbers <- 1       #the datasets to use
general_config$general$score <- FALSE                  #generate submission for Kaggle
general_config$general$id <- 2                        #id for saved configs and submissions, increase manually

general_config$sampling$half_max <- FALSE              #half the training set is picked from max effect, rest by random sampling

general_config$cause$remove_ineffective <- FALSE       #remove causes that does not show effect
general_config$cause$remove_col_threshold <- 5        #threshold where causes get disqualified if they don't generate effect
general_config$cause$remove_base_level_rows <- FALSE
general_config$cause$normalize <- FALSE

general_config$effect$recalibrate <- FALSE
general_config$effect$remove_baseline <- FALSE
general_config$effect$normalize <- FALSE

general_config$environmentals <- list()
general_config$environmentals$enabled <- TRUE
general_config$environmentals$enabled_list <- list('total', 'nonzero', 'meannonzero', 'numberover10', 'numberover20')  #enabled generated causes

general_config$rf$maxitems <- 40000000
general_config$rf$ntree  <- 100                       #trees to build
general_config$rf$maxnodes <- NULL                    #null for default options
general_config$rf$nodesize <- 25                      #null for default, 25 suggested by grid runs for decent acc and speed. lower it for best result.
general_config$rf$mtry <- 100                        #null for default options


#includes
source('lib/functions.R')
source('lib/environmentals.R')
source('lib/errorHandlers.R')
source('lib/dataLoaders.R')
source('lib/dataPreparation.R')
source('lib/dataWriters.R')
source('lib/ais.R')



#dev_run -> change options for a quick run through
if(general_config$general$dev_run){
  general_config$rf$ntree  <- 10                       #trees to build
  general_config$general$source_data_numbers <- 1:2      #just use the first two dataset to see all functions
}


#load and prepare source data
if(general_config$general$regen_data){source_data_dir_parser(general_config, sparse=T)}

#initiate progress indicator
loopstarttime=Sys.time()

#write progress data: percent done, time, expected time remaining, expected full time
#this has to be written as a file because the multithreaded loop can't display
writeProgress(new=TRUE)

###generate extra training data and train AIs####
report <- list()
source_data_numbers <- general_config$general$source_data_numbers
loop_count <- list(one_max=length(source_data_numbers), two_max=length(general_config$general$enabled_ais))
loop_count$one_now <- 0
for(h in source_data_numbers){
  gc()                                                                      #force garbage collection before the new set is run
  loop_count$one_now = loop_count$one_now + 1
  report[[h]] <- list()
  config <- select_config(h, general_config, special_config)                #if there is an individual configuration for this data set, use it, else use the general config
  load(file=paste(config$general$parsed_data_dir, get_source_data_name(h), '.Rdata', sep=""))  #load preparsed data
  
  
  ####identify and remove desciptors that does not give an effect?####
  if(config$cause$remove_ineffective){
    training_set <- source_data_remove_ineffective(config, training_set)
  }

  
  #####downsample if the data set is too large for memory (when going to random forest) ############
  if((training_set$ncols*training_set$nrows) > config$rf$maxitems){
    training_set <- source_data_downsample(config, training_set)
    gc()                                                                    #garbage collect, you just downsampled
  }


  ####make small test set for development####
  if(config$general$dev_run){ training_set <- data_shrink_for_dev(training_set)
    gc()
  }
  

  training_set$cause <- as.matrix(training_set$cause)           #go to dense matrix form


####decorate training set with refined causes (environmentals)####
if(config$environmentals$enabled) training_set$cause <- environmentals_get_cause(config, training_set$cause)
#plot groupings
#run a glm on this set and effect?
#join with original set?

  ####rework cause and effect, normalize, recalibrate?####
  if(config$effect$recalibrate){
    training_set$baseline <- min(training_set$effect)
    training_set$scalefactor <- max(training_set$effect)-min(training_set$effect)
    if(config$effect$remove_baseline) training_set$effect <- training_set$effect-min(training_set$effect) 
    if(config$effect$normalize) training_set$effect <- training_set$effect/training_set$scalefactor
    
    #recalibrate
    if(config$cause$normalize){
      for(i in 1:length(training_set$effect)){training_set$cause[i,] <- training_set$cause[i,] / training_set$effect[i]}
      training_set$effect <- rep(1, length(training_set$effect))           #remove baseline
    }
  }
    
    
    
  ####loop splits if we decide to include unsampled data, no splits yet####
  
  
  ####call AI training####
  
  #run the ais, store performance descriptors, parallellize if RAM can handle it
  loop_count$two_now <- 0
  for(ai_i in config$general$enabled_ais){
    loop_count$two_now = loop_count$two_now + 1
    report[[h]][[ai_i]] <- list()
    gc()                 #force garbage collection to free memory
    #try changing mtry, node, maxnodes
    for(grid in list(100)){
      config$rf$mtry <- grid
      mini_timer <- system.time(
        temp <- ai_one(config, training_set$cause, training_set$effect, ai_i, coef=F, error=T, model=T) #train ai
      )
      report[[h]][[ai_i]] <- list()
      report[[h]][[ai_i]][[grid]] <- temp                                             #report performance and store model
      message(h, ' - ', ai_i, ' - mtry ', grid, ' : Rsqr : ', round(temp$error, 5), ', used time: ', round(mini_timer[3], 5))
    }
  }
  
    
  
  ####generate prediction and write predictions for the test sets####
#TODO: if training cause and effect was manipulated, this needs to be done here as well and reversed for the submitted prediction
  if(config$general$score){
    if(!exists('submission')) submission <- NULL
    test_set <- list()
    #maybe run this in chunks and display progress?
    #select minimum Rsqr model
    testFile <- paste(config$general$test_set_dir, get_source_data_name(h, training=F), '.csv', sep="")
    test <- read.csv(testFile, header=TRUE)                         #read in test set
    test_set$cause <- test[,2:length(test)]
    #do the same preparation of the test set as was done to the training set
    if(config$environmentals$enabled) test_set$cause <- environmentals_get_cause(config, test_set$cause)
    
    
    test_set$molecule_names <- test[,1]
    test_set$prediction <- predict(temp$model, test_set$cause, type="response")         #predict
    submission <- append_test_prediction(config, test_set, submission)                  #append prediction to data set
    write_test_prediction(config, submission)                                           #generate submission file every pass so it is stored if the scripts crash
  }


  
  #### write data ####
  filename <- paste(config$general$generated_data_dir, 'ai_report', config$general$id, '.Rdata', sep="")
  save(list = "report", file=filename)
  filename <- paste(config$general$generated_data_dir, 'config', config$general$id, '.Rdata', sep="")
  save(list = c("general_config", "special_config"), file=filename)  
  share_done = (loop_count$one_now-1 + (loop_count$two_now / loop_count$two_max)) / loop_count$one_max 
  writeProgress(share_done, loopstarttime)
  messageProgress(share_done, loopstarttime)
  
}
rm(h, share_done)

message('finished prediction loop, report size is ', object.size(report))

endtime=Sys.time()
elapsedtime=endtime-loopstarttime
print(elapsedtime)

#write ending in progress file if this is edited remotely
writeProgress(1, loopstarttime)

#write data
#save(list = "training_set", file="generatedData/ai_training_set_raw.Rdata")
message('all finished')