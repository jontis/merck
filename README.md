# README - Kaggle.com machine learning competition - Merck Molecular Activity Challenge:

Components:
========================================
* AI evaluation system: Machine Learning systems are used for predicting system behaviour. The system to be predicted comes from laboratory data in Mercks drug development cycle and the goal is to predict activity for molecules with known features. The result is crossvalidated to predict the accuracy of the models, *(currently Generalized Linear Model, Generalized Boosted Regression Model, Random Forest, Support Vector Machine and an Artificial Neuron Net)*. The trained model then predicts response for a test set and writes a file with the data for verification at Kaggle.com

The best prediction system (bin/ai_stripped_ensemble.R) scored an 81st place on the private leaderboard out of 238 teams.

HOWTO:
=======================================
Place csv files with source data given from Kaggle in the source data folder, containing molecular activity data with with header row. The data shape is in the form [Name], [Activity], [feature1], ..., [featureLast]. Everything is run from main scripts in folder 'bin'. Configure the scripts and run. All main scripts can be run either from shell, R or RStudio if the required R packages are installed. Note that a package installed in R is only available from shell and R, not in RStudio, and packages installed in RStudio is not available from shell or R.
